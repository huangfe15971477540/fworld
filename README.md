
# PPC-Web-Content
[![Language](https://img.shields.io/badge/Language-Vue-blue.svg)](https://vuejs.org/index.html)
Web content of the Privacy-Preserving Computation platform
## 主要功能
* 平台首页：展示机构和数据集和任务的概览状况，以及各个服务的运行状态
* 机构管理：注册机构，查看和编辑机构信息，删除机构。
* 数据管理：用户上传数据集，授权给合作方使用数据集。
* 算法管理：用户上传算法来支撑各种类型的任务。
* 任务关联：用户创建任务，查看任务执行状态，获取任务结果。
## 安装依赖
```
npm install
```
## 运行
```
npm run dev
```
## 打包
```
npm run build:prod
```
## 部署
##### 打包出来dist目录部署在认证服务 /ppc/auth-service目录下，并将dist重命名为pages。

## 体验

##### 微众银行多方大数据隐私计算平台WeDPR-PPC已开放核心功能体验，提供核心安全多方计算引擎和常用隐私计算应用模板，助力合作伙伴在可快速迭代的实验环境中，探索隐私计算的实际效果。欢迎通过“小助手”申请体验：


 ![image](./taste.jpg)
 

